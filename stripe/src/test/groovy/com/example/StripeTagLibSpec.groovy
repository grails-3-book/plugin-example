package com.example

import grails.testing.web.taglib.TagLibUnitTest
import spock.lang.Specification

class StripeTagLibSpec extends Specification implements TagLibUnitTest<StripeTagLib> {

    def setup() {
    }

    def cleanup() {
    }

    void "checkout displays correctly with defaults"() {
        when:
        def output = tagLib.checkout(attr).toString()

        then:
        output == """<form action="" method="POST">
  <script
          class="stripe-button"
          src="https://checkout.stripe.com/checkout.js"
          data-key=""
          data-amount="10000"
          data-name="Widget"
          data-description=""
          data-image=""
          data-locale="auto"
          data-zip-code="true">
  </script>
</form>"""

        where:
        attr = [name: "Widget", amount: "10000"]
    }

    void "checkout display non-defaults"() {
        when:
        def output = tagLib.checkout(attr).toString()

        then:
        output == """<form action="" method="POST">
  <script
          class="stripe-button"
          src="https://checkout.stripe.com/checkout.js"
          data-key=""
          data-amount="10000"
          data-name="Widget"
          data-description="foobar"
          data-image="foo.png"
          data-locale="auto"
          data-zip-code="false">
  </script>
</form>"""

        where:
        attr = [name: "Widget",
                amount: "10000",
                description: "foobar",
                image: "foo.png",
                verifyZip: false]
    }
}
