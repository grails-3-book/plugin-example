package com.example

import com.stripe.exception.CardException
import com.stripe.model.Charge
import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class StripeControllerSpec extends Specification implements ControllerUnitTest<StripeController> {

    def setup() {
        GroovySpy(Charge, global: true)
    }

    def cleanup() { }

    void "Test charge succeed"() {
        given:
        params.amount = 10000

        and:
        GroovySpy(Charge)

        when:
        controller.charge()

        then:
        1 * Charge.create(_) >> [id: 1]

        and:
        response.forwardedUrl == '/stripe/thankYou?amount=10000'
    }

    void "Test charge fail"() {
        given:
        params.amount = 10000

        and:
        Charge.create(_) >> {
            throw new CardException('message', 'requestId', '400', 'param', '123', 'ch_123', 400, new Exception())
        }

        when:
        controller.charge()

        then:"Exceptions are caught"
        notThrown(CardException)

        and:"forwarded to error action"
        response.forwardedUrl == '/stripe/error?amount=10000'
    }
}
