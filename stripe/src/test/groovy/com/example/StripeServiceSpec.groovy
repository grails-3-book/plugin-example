package com.example

import grails.events.bus.EventBus
import grails.testing.services.ServiceUnitTest
import spock.lang.Specification

class StripeServiceSpec extends Specification implements ServiceUnitTest<StripeService> {

    def setup() {
        service.setTargetEventBus(Mock(EventBus))
    }

    def cleanup() { }

    void "Notification is correctly formatted"() {
        when:
        service.notifyWebHook([type:type, foo:"bar"])

        then:
        1 * service.eventBus.notify("stripe.${type}", _)

        where:
        type = 'charge.succeeded'
    }

    void "Notification is correctly handles null"() {
        when:
        service.notifyWebHook([type:null, foo:"bar"])

        then:
        1 * service.eventBus.notify('stripe.unknown', _)
    }
}
