package com.example

import grails.events.EventPublisher

class StripeService implements EventPublisher {
    def notifyWebHook(def payLoad) {
        def type = payLoad?.type ?: 'unknown'

        notify("stripe.${type}", payLoad) // <1>
    }
}
