package com.example

import com.stripe.Stripe
import com.stripe.model.Charge

class StripeController {

    def stripeService

    // tag::chargeEndpoint[]
    def charge() {
        Stripe.apiKey = grailsApplication.config.stripe.apiKey
        String token = params.stripeToken

        Map chargeParams = [
                amount: params.int('amount'),
                currency: 'usd',
                description: params.item,
                source: token,
                metadata: [:] // <1>
        ]

        def charge // <2>
        try {
            charge = Charge.create(chargeParams)
        } catch (e) {
            log.error '{}', e
        }

        if (charge?.id) {
            forward action: 'thankYou'
        } else {
            forward action: 'error'
        }
    }

    def thankYou() {
        render 'Thank You for your purchase!'
    }

    def error() {
        render 'Payment failed!'
    }
    // end::chargeEndpoint[]

    // tag::webHookEndpoint[]
    def webHook() {
        if(request.JSON) {
            stripeService.notifyWebHook(request.JSON)
        }
        render '' // <2>
    }
    // end::webHookEndpoint[]
}
