package com.example

class StripeTagLib {
    static defaultEncodeAs = [taglib:'none']
    static namespace = 'stripe'

    /**
     * Renders a stripe checkout form
     *
     * attr amount REQUIRED The amount of the item in *cents*
     * attr name REQUIRED The name of the item
     * attr description The description of the item
     * attr image An image of the item
     * attr verifyZip require zip-code verification. Default `true`
     * attr callBackUrl the url the form will submit to after payment
     */
    def checkout = { attrs ->
        String stripePublicKey = grailsApplication.config.stripe.publicKey
        String image = attrs?.image ?: grailsApplication.config.stripe.defaultImage
        String callBackUrl = attrs?.callBackUrl ?:  grailsApplication.config.stripe.defaultCallBackUrl
        Boolean verifyZip = attrs?.verifyZip == null ? true : attrs?.verifyZip

        out << render(template: "/stripe/checkout",
                model: [
                        attrs: attrs,
                        image: image,
                        verifyZip: verifyZip,
                        callBackUrl: callBackUrl,
                        stripePublicKey: stripePublicKey
                ])
    }
}
